"""Module contains functions needed for schedule monitoring"""

import re
import sys
import urllib.request
import logging

from re import Pattern
from lxml import etree
from openpyxl import Workbook, load_workbook

DAYS = ["понедельник", "вторник", "среда", "четверг", "пятница", "суббота"]
CLASS_TYPES = ["л", "пз", "л/пз"]

BASE_URL = "https://sevsu.ru"
BASE_SCHEDULE_URL = "https://www.sevsu.ru/univers/shedule/"

TEMP_SCHEDULE_FILE_NAME = "schedule.xlsx"

BROWSER_USER_AGENT = (
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) "
    "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36")

GROUP_NAME_PATTERN = r"[А-Яа-я]+(\/[бмс])?-\d+(-\d+)*-о"
CLASS_NUMBER_PATTERN = r"(\d)\s*пара"

def create_browser_agent_request(url: str):
    """Return urllib.request.Request with browser-like user agent"""

    request = urllib.request.Request(url)
    request.add_header("User-agent", BROWSER_USER_AGENT)
    return request

def get_schedule_file_url(institute: str, course: str):
    """Return schedule file url for institute and course"""

    schedule_file_link_xpath = \
        f"//div[div[h4[text()='{institute}']]]/div//div//a[div[contains(text(), '{course}')]]"

    with urllib.request.urlopen(create_browser_agent_request(BASE_SCHEDULE_URL)) as stream:
        html_content = stream.read().decode("utf-8")
        tree = etree.fromstring(html_content, etree.XMLParser(recover=True))
        link = tree.xpath(schedule_file_link_xpath)[0]
        return link.attrib["href"]

def download_schedule_file(download_url: str, output_file_path: str):
    """Download file from download_url and write to output_file_path"""

    full_download_url = BASE_URL + download_url
    with \
        urllib.request.urlopen(create_browser_agent_request(full_download_url)) as stream, \
        open(output_file_path, 'wb') as schedule_file:

        schedule_file.write(stream.read())

def parse_group_position(row, group_matcher: Pattern, group: str):
    """Return column position of group"""

    pos = 0
    for cell in row:
        if not isinstance(cell, str):
            continue
        if not group_matcher.match(cell):
            continue
        pos += 1
        if group in cell:
            return pos

    return None

def parse_day_start(row):
    """Return week day in row, if presented"""

    for cell in row:
        if not isinstance(cell, str):
            continue

        stripped_cell = cell.strip().lower()
        if stripped_cell in DAYS:
            return stripped_cell

    return None

def parse_schedule_row(row, class_number_matcher: Pattern, pos: int):
    """Return class description in this row for column in pos, if presented"""

    cols_to_skip = pos - 1 # skip other groups

    cells_to_skip = 0
    class_number = None
    class_title = None
    class_type = None
    class_location = None

    for cell in row:
        if not isinstance(cell, str):
            continue

        if cells_to_skip > 0:
            cells_to_skip -= 1
            continue

        if cols_to_skip == 0 and \
            class_title is not None and class_type is not None and class_location is not None:
            return {
                "number": class_number,
                "title": class_title,
                "type": class_type,
                "location": class_location
            }

        stripped_cell = cell.strip().lower()

        matched_class_number = class_number_matcher.match(cell)
        if matched_class_number:
            class_number = int(matched_class_number.group(1))
            class_title = None
            class_type = None
            class_location = None
            cells_to_skip = 1 # skip class start time

            if cols_to_skip > 0:
                cols_to_skip -= 1

            continue

        if class_number is None:
            continue

        if cell != "" and class_title is None:
            class_title = cell
            continue

        if class_title is None:
            continue

        if class_type is None and stripped_cell in CLASS_TYPES:
            class_type = stripped_cell
            continue

        if class_location is None and cols_to_skip == 0:
            class_location = cell
            continue

    return None

def parse_schedule(schedule_workbook: Workbook, group: str):
    """Return array of week schedules for group"""

    group_name_matcher = re.compile(GROUP_NAME_PATTERN)
    class_number_matcher = re.compile(CLASS_NUMBER_PATTERN)

    schedule = []

    for worksheet in schedule_workbook.worksheets:
        logging.info("Parsing worksheet '%s'", worksheet.title)

        parsed_position = None
        current_day = None
        day_schedule = []
        week_schedule = {}

        for row in worksheet.iter_rows(values_only=True):

            if parsed_position is None:
                parsed_position = parse_group_position(row, group_name_matcher, group)
                continue

            parsed_day = parse_day_start(row)
            if current_day != parsed_day and current_day is not None:
                week_schedule[current_day] = day_schedule
                day_schedule = []
            current_day = parsed_day

            parsed_class = parse_schedule_row(row, class_number_matcher, parsed_position)

            if parsed_class is not None:
                day_schedule.append(parsed_class)

        schedule.append(week_schedule)

    return schedule


logging.root.setLevel(logging.INFO)

logging.info("Finding schedule file download url ...")
schedule_url = get_schedule_file_url("Институт развития города", "1 курс Магистратура")
if schedule_url is None:
    logging.info("Download url not found!")
    sys.exit(1)

logging.info("Download url found: %s", schedule_url)

logging.info("Downloading schedule file ...")
download_schedule_file(schedule_url, TEMP_SCHEDULE_FILE_NAME)
logging.info("File downloaded successfully")

wb = load_workbook(TEMP_SCHEDULE_FILE_NAME)
group_schedule = parse_schedule(wb, "Г/м-22-1-о")

print(group_schedule)
